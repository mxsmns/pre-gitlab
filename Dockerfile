# syntax=docker/dockerfile:1.4
FROM python:3.6-slim AS pre-commit

ARG ci_build=/builds/mxsmns/pre-gitlab
ENV PRE_COMMIT_HOME=/pre_commit

RUN : \
	&& apt-get update \
	&& DEBIAN_FRONTEND=noninteractive apt-get install -y \
		--no-install-recommends \
		git \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/* \
	&& :

RUN pip install --no-cache-dir pre-commit

FROM pre-commit AS installed
RUN mkdir -p /builds/mxsmns/pre-gitlab
COPY --link .pre-commit-config.yaml /builds/mxsmns/pre-gitlab/.pre-commit-config.yaml
WORKDIR /builds/mxsmns/pre-gitlab
RUN : \
    && mkdir -p $PRE_COMMIT_HOME \
    && git init .\
    && pre-commit install-hooks \
    && :

FROM pre-commit
RUN mkdir -p $PRE_COMMIT_HOME
COPY --from=installed $PRE_COMMIT_HOME $PRE_COMMIT_HOME
